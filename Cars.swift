

enum engineState {
    case start, stop
}

enum windowPosition {
    case open, close
}

enum trunkFullness {
    case full, empty
}

struct someCar {
    let brand : String
    let releaseYear : Int
    
    var trunkVolume : Int {
        willSet {
            if (trunkFullness == .empty) && (trunkVolume > 0) && (trunkVolume != 0) && (newValue < trunkVolume) {
                let space = trunkVolume - newValue
                print ("В легковой \(brand) осталось места: \(space)")
            } else { print("У легковой \(brand) багажник полон")}
        }
    }
    var engineState : engineState {
        willSet {
            if newValue == .start {
                print ("У легковой \(brand) двигатель заведен")
            } else {print("У легковой \(brand) двигатель заглушен")}
        }
    }
    var windowPosition : windowPosition {
        willSet {
            if newValue == .open {
                print("У легковой \(brand) открыты окна")
            } else { print("У легковой \(brand) закрыты окна") }
        }
    }
    var trunkFullness : trunkFullness
    mutating func emptyTrunck() {
        self.trunkFullness = .empty
        print ("У легковой \(brand) багажник пустой")
    }
}

struct someTruck {
    let brand : String
    let releaseYear : Int
    
    var trunkVolume : Int {
        willSet {
            if (trunkFullness == .empty) && (trunkVolume > 0) && (trunkVolume != 0) && (newValue < trunkVolume) {
                let space = trunkVolume - newValue
                print ("У грузовика \(brand) осталось места: \(space)")
            } else { print("У грузовика \(brand) полный багажник.")}
        }
    }
    var engineState : engineState {
        willSet {
            if newValue == .start {
                print ("У грузовика \(brand) двигатель заведен")
            } else {print("У грузовика \(brand) двигатель заглушен")}
        }
    }
    var windowPosition : windowPosition {
        willSet {
            if newValue == .open {
                print("У грузовика \(brand) открыты окна")
            } else { print("У грузовика \(brand) закрыты окна") }
        }
    }
    var trunkFullness : trunkFullness
    mutating func emptyTrunck() {
        self.trunkFullness = .empty
        print ("У грузовика \(brand) багажник пустой")
    }
}

var car1 = someCar(brand: "BMW", releaseYear: 2016, trunkVolume: 580, engineState: .stop, windowPosition: .open, trunkFullness: .empty)

car1.engineState = .start
car1.trunkVolume = 340

var car2 = someCar(brand: "Audi", releaseYear: 2010, trunkVolume: 400, engineState: .stop, windowPosition: .close, trunkFullness: .full)

car2.engineState = .stop
car2.windowPosition = .open

var truck1 = someTruck(brand: "KAMAZ", releaseYear: 2014, trunkVolume: 11000, engineState: .start, windowPosition: .close, trunkFullness: .full)

truck1.engineState = .stop
truck1.trunkFullness = .empty
truck1.trunkVolume = 1000

var truck2 = someTruck(brand: "MAN", releaseYear: 2010, trunkVolume: 40000, engineState: .start, windowPosition: .open, trunkFullness: .full)

truck2.engineState = .stop
truck2.windowPosition = .close
