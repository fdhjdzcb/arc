// Capture List (список захвата) в Swift - это механизм, который позволяет лямбда-выражениям или замыканиям захватывать значения из окружающей области видимости.

let multiplier = 10
let closure: () -> Int = { [multiplier] in
    return multiplier * 2
}
print(closure()) // 20